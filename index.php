<html>
<head>
	<title>Aplikasi Undian</title>

	<style type="text/css">

		body {
			font-family: sans-serif;
		}

		.container {
			width: 900px;
			margin: 0 auto;
			padding: 16px;
			box-shadow: 0 4px 8px #DDD;
			text-align: center;
		}

		.container h1 {
			text-align: center;
		}

		.action {
			margin-top: 16px;
		}

		.random-indicator {
			margin-top: 16px;
			font-size: 18pt;
			padding: 8px;
		}

		.winner {
			margin-top: 16px;
			font-size: 24pt;
			font-weight: bold;
			padding: 8px;
			background: #2DABF9;
			color: white;
		}

		#user_sum {
			font-weight: bold;
		}

		#winner,
		#random_indicator,
		#user_sum,
		#action {
			display: none;
		}
	</style>
</head>
<body>

	<div class="container">
		<h1>UNDIAN NASABAH BERHADIAH</h1>
		
		<div id="user_sum"><!-- Jumlah data --></div>

		<div id="action" class="action">
			<button onclick="start()">Mulai Undi</button>
			<button onclick="stop()">Ambil Pemenang</button>
		</div>

		<div id="random_indicator" class="random-indicator"><!-- ketika proses undi disini --></div>

		<div id="loading" ><img src="loader.gif" alt="loading..." width="128" /></div>
		<div id="winner" class="winner"><!-- Pemenang --></div>

		
	</div>

	<script type="text/javascript" src="jquery.min.js"></script>
	<script type="text/javascript">
		userData = [];


		$(document).ajaxStart(function() {
			$('#loading').show();
		}).ajaxStop(function() {
			$('#loading').hide();
			$('#user_sum').fadeIn('slow');
		});

		$(document).ready(function() {
			$.ajax({
				type: "GET",
				url: 'data.php',
				data: [],
				dataType : "json",
				success: function(data) {
					$('#user_sum').show();
					$('#user_sum').html("Jumlah Nasabah : " + data.total);
					$('#action').show();

					userData = data;
				}
			});
		});


		var timer, 
			finish 		= 1000, 
			currentData = "",
			randomNumber= 0,
			sequence 	= 0;

		function countdown() {
			displayRandom();
			//console.log(currentData, finish, sequence, randomNumber);
			if(currentData === 0 || finish == 0) {
				stop();
				finish = 1000;
			} else {
				finish--;
				randomNumber 	= Math.floor((Math.random() * userData.total) + 1); // random data between 1 until user count
				currentData 	= userData.data[randomNumber];
				sequence++;

				if(sequence == userData.total) sequence = 0;

				timer = setTimeout(countdown, 10);
			}
		}

		function displayRandom() {
			$("#random_indicator").html(currentData);
		}

		function start() {
			finish 		= 1000; // init finish timer
			$("#random_indicator").show();
			$("#winner").hide();
			countdown();

		}

		function stop() {
			$("#random_indicator").hide();
			$("#winner").fadeIn("slow");
			$("#winner").html(currentData);
			clearTimeout(timer);
		}


	</script>
	
</body>
</html>